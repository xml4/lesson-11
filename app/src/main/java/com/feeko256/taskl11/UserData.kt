package com.feeko256.taskl11

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class UserData(
    val firstName: String? = null,
    val lastName: String? = null,
    val middleName: String? = null,
    val age: Int? = null,
    val hobby: String? = null,
) : Parcelable
package com.feeko256.taskl11

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class InfoActivity : AppCompatActivity() {

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val userData = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent.getParcelableExtra("userData", UserData::class.java)
        } else {
            intent.getParcelableExtra<UserData>("userData")
        }
        if (userData != null) {
            generateText(userData)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            finish()
        return true
    }

    private fun generateText(userData: UserData) {
        when (userData.age) {
            in Int.MIN_VALUE..0 -> {
                errorAgeText(userData)
            }

            in 1..15 -> {
                lowAgeText(userData)
            }

            in 16..25 -> {
                middleAgeText(userData)
            }

            else -> {
                heightAgeText(userData)
            }
        }
    }

    private fun lowAgeText(userData: UserData) {
        val userText = findViewById<TextView>(R.id.userTextView)
        var text =
            "Привет ${userData.firstName} ${userData.lastName} ${userData.middleName}." +
                    " Тебе всего ${userData.age}," +
                    " поэтому я рад что тебе нравится ${userData.hobby}"
        userText.text = text
    }

    private fun middleAgeText(userData: UserData) {
        val userText = findViewById<TextView>(R.id.userTextView)
        var text =
            "Привет ${userData.firstName} ${userData.lastName} ${userData.middleName}." +
                    " Тебе уже ${userData.age}," +
                    " мне кажется, что лучше заниматься учебой чем ${userData.hobby}"
        userText.text = text
    }

    private fun heightAgeText(userData: UserData) {
        val userText = findViewById<TextView>(R.id.userTextView)
        var text =
            "Привет ${userData.firstName} ${userData.lastName} ${userData.middleName}." +
                    " Вам уже ${userData.age}," +
                    " нет ничего лучше чем ${userData.hobby} на старости лет"
        userText.text = text
    }

    private fun errorAgeText(userData: UserData) {
        val userText = findViewById<TextView>(R.id.userTextView)
        var text =
            "Привет ${userData.firstName} ${userData.lastName} ${userData.middleName}." +
                    " мне кажется вам не может быть ${userData.age},"
        userText.text = text
    }
}




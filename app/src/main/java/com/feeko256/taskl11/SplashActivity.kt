package com.feeko256.taskl11

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.feeko256.taskl11.R
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@SuppressLint("CustomSplashScreen")
class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        GlobalScope.launch {
            startMainActivity()
        }
    }
    suspend fun startMainActivity(){
        delay(3000)
        val intent = Intent(this,  MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
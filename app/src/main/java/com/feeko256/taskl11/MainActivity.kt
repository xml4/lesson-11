package com.feeko256.taskl11

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.constraintlayout.widget.ConstraintLayout

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        randomizeBackground()

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.sync -> saveData()
            R.id.pic -> randomizeBackground()
            R.id.liveData -> activityWithLiveData()
            R.id.load -> loadData()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun saveData() {
        val nameText = findViewById<EditText>(R.id.nameText)
        val lastNameText = findViewById<EditText>(R.id.lastNameText)
        val middleNameText = findViewById<EditText>(R.id.middleNameText)
        val ageText = findViewById<EditText>(R.id.ageText)
        val hobby = findViewById<EditText>(R.id.hobbyText)

        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply() {
            putString("nameText", nameText.text.toString())
            putString("lastNameText", lastNameText.text.toString())
            putString("middleNameText", middleNameText.text.toString())
            putInt("ageText", ageText.text.toString().toInt())
            putString("hobby", hobby.text.toString())
        }.apply()
    }

    private fun loadData() {
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)

        val user = UserData(
            firstName = sharedPreferences.getString("nameText", null),
            lastName = sharedPreferences.getString("lastNameText", null),
            middleName = sharedPreferences.getString("middleNameText", null),
            age = sharedPreferences.getInt("ageText", 0),
            hobby = sharedPreferences.getString("hobby", null),
        )
        val intent = Intent(this, InfoActivity::class.java)
        intent.putExtra("userData", user)
        startActivity(intent)
    }

    private fun randomizeBackground() {
        val conLayout = findViewById<ConstraintLayout>(R.id.conLayout)
        when ((0..6).random()) {
            0 -> conLayout.setBackgroundResource(R.drawable.p1)
            1 -> conLayout.setBackgroundResource(R.drawable.p2)
            2 -> conLayout.setBackgroundResource(R.drawable.p3)
            3 -> conLayout.setBackgroundResource(R.drawable.p4)
            4 -> conLayout.setBackgroundResource(R.drawable.p5)
            5 -> conLayout.setBackgroundResource(R.drawable.p6)
            6 -> conLayout.setBackgroundResource(R.drawable.p7)
        }
    }

    private fun activityWithLiveData() {
        startActivity(Intent(this, LiveDataActivity::class.java))
    }

    public fun acceptInfoButtonClick(view: View) {
        val nameText = findViewById<EditText>(R.id.nameText)
        val lastNameText = findViewById<EditText>(R.id.lastNameText)
        val middleNameText = findViewById<EditText>(R.id.middleNameText)
        val ageText = findViewById<EditText>(R.id.ageText)
        val hobby = findViewById<EditText>(R.id.hobbyText)

        val user = UserData(
            firstName = nameText.text.toString(),
            lastName = lastNameText.text.toString(),
            middleName = middleNameText.text.toString(),
            age = ageText.text.toString().toInt(),
            hobby = hobby.text.toString(),
        )
        val intent = Intent(this, InfoActivity::class.java)
        intent.putExtra("userData", user)
        startActivity(intent)
    }
}
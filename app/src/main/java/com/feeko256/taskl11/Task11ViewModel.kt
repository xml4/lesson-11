package com.feeko256.taskl11

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class Task11ViewModel : ViewModel() {

    var number = 0
    val currentNumber: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }
    val currentBoolean: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }
}
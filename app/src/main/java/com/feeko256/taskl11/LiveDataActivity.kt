package com.feeko256.taskl11

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import org.w3c.dom.Text

class LiveDataActivity : AppCompatActivity() {
    lateinit var viewModel: Task11ViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live_data)

        viewModel = ViewModelProvider(this).get(Task11ViewModel::class.java)
        val tview1 = findViewById<TextView>(R.id.textView)
        val tview2 = findViewById<TextView>(R.id.textView2)

        viewModel.currentNumber.observe(this, Observer {
            tview1.text = it.toString()
        })
        viewModel.currentBoolean.observe(this, Observer {
            tview2.text = it.toString()
        })
    }

    fun incrementText(view: View) {
        viewModel.currentNumber.value = ++viewModel.number
        viewModel.currentBoolean.value = viewModel.number % 2 == 0
    }
}